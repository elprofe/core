package org.ssldev.core.utils;

import org.junit.Test;
import org.ssldev.core.messages.Message;
import org.ssldev.core.messages.MessageIF;

public class ValidateTest {


	private interface TestIF {
		@SuppressWarnings("unused")
		default public void test() {}
	}
	private class TestClass implements TestIF {
	}
	
	@Test public void isAssignable() {
		String[] strs = {"a","b",""};
		Validate.isAssignableFrom(strs, String.class);
		
		TestClass[] classes = {new TestClass()};
		Validate.isAssignableFrom(classes, TestIF.class);
		Validate.isAssignableFrom(classes, TestClass.class);
	}
	@Test public void isAssignable_clz() {
		Class<?>[] strs = {"a".getClass(),"b".getClass(),"".getClass()};
		Validate.areClassesAssignableFrom(strs, String.class);
	}
	@Test public void isAssignable_Empty() {
		String[] strs = {};
		Validate.isAssignableFrom(strs, String.class);
	}
	@Test public void isAssignable_Null() {
		Validate.isAssignableFrom(null, TestIF.class);
	}
	
	private static class TestMessage extends Message {}
	
	@Test public void classesThatExtendMessage_areAssignableFromMessageIF() {
		Class<?>[] msgs = {TestMessage.class};
		Validate.areClassesAssignableFrom(msgs, MessageIF.class);
	}

}
