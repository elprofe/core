package org.ssldev.core.utils;

import static java.util.Arrays.asList;

import java.io.File;

public class Validate {

	/**
	 * validates that the given object is not null
	 * @param o obj to validate
	 * @param err text to display when null
	 * @throws IllegalArgumentException if null
	 */
	public static void notNull(Object o, String err) throws IllegalArgumentException {
		if(null == o) throw new IllegalArgumentException(err);
	}

	public static void isTrue(boolean b, String err) throws IllegalArgumentException {
		if(!b) throw new IllegalArgumentException(err);
	}

	public static void isFalse(boolean b, String err) throws IllegalArgumentException {
		if(b) throw new IllegalArgumentException(err);
	}

	/**
	 * validates that the given file is not null and exists.
	 * @param f file
	 */
	public static void exists(File f) {
		notNull(f, "file cannot be null");
		isTrue(f.exists(), f.getAbsolutePath() + " does not exist!");
	}

	/**
	 * validates that every object in the given array is assignable from the given class.
	 * parameter.
	 * <p>
	 * NOTE: the check is a reverse of how {@link Class#isAssignableFrom} is performed.
	 *   
	 * @param arr to check (okay to be null or empty)
	 * @param clz to verify assignability from
	 */
	public static void isAssignableFrom(Object[] arr, Class<?> clz) {
		if(null != arr) {
			notNull(clz, "class type cannot be null");
			asList(arr).forEach(m -> isTrue(clz.isAssignableFrom(m.getClass()), m.getClass().getName()+" is not assignable from "+clz.getName()));
		}
	}

	/**
	 * validates that every class in the given array of classes is assignable from 
	 * the given class parameter.
	 * <p>
	 * NOTE: the check is a reverse of how {@link Class#isAssignableFrom} is performed.
	 *   
	 * @param classes to check (okay to be null or empty)
	 * @param clz to verify assignability from
	 */
	public static void areClassesAssignableFrom(Class<?>[] classes, Class<?> clz) {
		if(null != classes) {
			notNull(clz, "class type cannot be null");
			asList(classes).forEach(m -> isTrue(clz.isAssignableFrom(m), m+" is not assignable from "+clz.getName()));
		}
	}

}
