package org.ssldev.core.messages;

public interface MessageIF {

	public default String getName() {
		return this.getClass().getSimpleName();
	}
}
