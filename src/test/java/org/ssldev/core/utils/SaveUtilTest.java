package org.ssldev.core.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.Serializable;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.ssldev.core.utils.SaveUtil;

public class SaveUtilTest {
	private SaveUtil util = SaveUtil.getInstance();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSimpleSaveAndLoad() {
		// save data
		TestService t = new TestService ();
		t.data.put("dog", "barks");

		try {
			util.save(t.getClass().getSimpleName(),t);
		} catch (Exception e) {
			fail("failed to save due to " + e);
		}
		
		// load back data
		List<Serializable> savedData = util.load(t.getClass().getSimpleName());
		assertNotNull(savedData);
		assertFalse(savedData.isEmpty());
		
		Serializable data = savedData.get(0);
		assertTrue(data instanceof TestService);
		TestService t2 = (TestService)data;
		
		assertNotNull(t2.data);
		assertEquals(t2.data.get("dog"),"barks");
		
		SaveUtil.getInstance().delete(t.getClass().getSimpleName());
		
	}

}
