package org.ssldev.core.messages;

import org.ssldev.core.utils.NotLoggableIF;

public class LogMessage extends Message implements NotLoggableIF{
	public String entry;
	
	public LogMessage(String txt) {
		entry = txt;
	}
}
