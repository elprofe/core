package org.ssldev.core.messages;

import java.util.List;

/**
 * file read and its contents are stored in the list parameter
 */
public class FileReadMessage extends Message {

	public List<String> content;
	public String fileName;

	public FileReadMessage(List<String> ret, String name) {
		this.content = ret;
		this.fileName = name;
	}

}
