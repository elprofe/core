package org.ssldev.core.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.ssldev.core.messages.FileReadMessage;
import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.messages.ReadFromFileMessage;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.utils.Logger;
/**
 * reads in file into a list of strings.  
 * 
 * Consumes: 
 * <ul>
 * <li>{@link ReadFromFileMessage}</li>
 * </ul>
 * Produces:
 * <ul>
 * <li>{@link FileReadMessage}</li>
 * </ul>
 */
public class FileReaderService extends Service {

	public FileReaderService(EventHub hub) {
		super(hub);
	}

	@Override
	public void notify(MessageIF msg) {
		if(msg instanceof ReadFromFileMessage) {
			ReadFromFileMessage readMsg = (ReadFromFileMessage) msg;
			readFile(readMsg.fileToRead);
		}
	}

	private void readFile(File fileToRead) {
		// can only handle csv file right now...
		if(!fileToRead.getName().endsWith("csv")) {
			Logger.warn(this, "can only handle csv files right now");
			return;
		}
		
		List<String> ret = new LinkedList<>();
		Scanner s = null;
		try {
			s = new Scanner(fileToRead);
			while(s.hasNextLine()) {
				ret.add(s.nextLine());
			}
		} catch (FileNotFoundException e) {
			Logger.warn(this, fileToRead.getAbsolutePath() + " was not found");
			e.printStackTrace();
			return;
		}
		finally {
			if(null != s) s.close();
		}
		
		hub.add(new FileReadMessage(ret, fileToRead.getName()));
	}

}
