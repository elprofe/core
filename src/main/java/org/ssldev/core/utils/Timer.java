package org.ssldev.core.utils;

import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * a timer that can schedule tasks to run after a given delay, or to execute periodically.
 */
public class Timer {
	
	
	/**
	 * execute the given task after the given delay.
	 * 
	 * @param task to execute
	 * @param delay to wait before executing the task
	 * @param unit of time of the delay parameter
	 * @return {@link Runnable} to call to cancel task execution 
	 */
	public static Runnable doIn(Runnable task, long delay, TimeUnit unit) {
		java.util.Timer t = new java.util.Timer();
		t.schedule(new Task(task), unit.toMillis(delay));
		return () -> t.cancel();
	}
	
	/**
	 * execute a periodic task subsequently with the given period.
	 * If any execution of the task encounters an exception, subsequent executions are 
	 * suppressed. Otherwise, the task can be terminated via ScheduledFuture cancel.
	 * 
	 * @param task to execute
	 * @param period of time to repeat task execution
	 * @param unit of time of the period parameter
	 * @return {@link Runnable} to call to cancel task execution 
	 */
	public static Runnable doEvery(Runnable task, long period, TimeUnit unit) {
		java.util.Timer t = new java.util.Timer();
		t.scheduleAtFixedRate(new Task(task), 0, unit.toMillis(period));
		return () -> t.cancel();
	}
	
	private static class Task extends TimerTask {

		private final Runnable taskToExec;

		public Task (Runnable taskToExec) {
			this.taskToExec = taskToExec;
		}
		
		@Override
		public void run() {
			taskToExec.run();
		}
	}

}
