package org.ssldev.core.services;

import static org.ssldev.core.utils.StringUtils.stackTraceToString;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.messages.WriteToFileMessage;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.utils.Logger;
/**
 * writes stuff to a file
 * 
 * Consumes: 
 * <ul>
 * <li>{@link WriteToFileMessage}</li>
 * </ul>
 * Produces: N/A
 */
public class FileWriterService extends Service {
	private HashMap<String,FileWriter> nameToWriter = new HashMap<>();
	
	public FileWriterService(EventHub hub) {
		super(hub);
	}

	@Override
	public void notify(MessageIF msg) {
		if(!(msg instanceof WriteToFileMessage)) return;
		
		WriteToFileMessage writeMsg = (WriteToFileMessage) msg;
		
		try {
			process(writeMsg.fileloc, writeMsg.content);
		} catch (IOException e) {
			Logger.error(this, "unable to write to file <"+writeMsg.fileloc+"> due to:\n"+stackTraceToString(e));
		}
	}

	private void process(String fileloc, String content) throws IOException {
		FileWriter fw = nameToWriter.get(fileloc);
		if(null == fw) {
			fw = new FileWriter(new File(fileloc));
			nameToWriter.put(fileloc, fw);
			fw.write(""); // overwrite previous data
			Logger.debug(this, "clearing out " + fileloc);
		}
		fw.append(content);
		fw.flush();
		Logger.finest(this, "appended " + content);
	}

	@Override
	public void shutdown() {
		super.shutdown();
			nameToWriter.values().forEach(fw-> 
			{
				try {
					fw.close();
				} catch (Exception e) {
					Logger.error(this, "error closing filewriter due to " + e.toString());
					e.printStackTrace();
				}}
			);
	}

}
