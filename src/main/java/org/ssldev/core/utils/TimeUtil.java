package org.ssldev.core.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * utilty for time related artifacts
 */
public class TimeUtil {
	
	private static final String MM_YYYY_REGEX = "(0?[1-9]|1[012])/((18|19|20|21)\\d\\d)";
	private static Pattern monthYearRegexPattern = Pattern.compile(MM_YYYY_REGEX);
	
	public static enum MONTH {
		JAN("01",31), FEB("02",28),MAR("03",31),APR("04",30),MAY("05",31),JUN("06",30),JUL("07",31),
		AUG("08",31),SEP("09",30),OCT("10",31),NOV("11",30),DEC("12",31);
		
		final String month;
		final int numDaysInMonth;
		
		private static Map<String,MONTH> monthByMonthNumber;
		static {
			monthByMonthNumber = Arrays.stream(MONTH.values())
				      .collect(Collectors.toMap(MONTH::getMonthString, Function.identity()));
		}
		
		private MONTH(String month, int  numDaysInMonth) {
			this.month = month;
			this.numDaysInMonth = numDaysInMonth;
		}
		private String getMonthString() {
			return month;
		}

		public static Optional<MONTH> of(String monthNum) {
			return  Optional.ofNullable(monthByMonthNumber.get(monthNum));
		}
	}
	
	/**
	 * convert the given time of epoch miliseconds into the equivalent {@link LocalDate} 
	 * @param epochTimeInMilis to convert
	 * @return {@link LocalDate} equivalent of given epoch time
	 */
	public static LocalDate toLocalDate(long epochTimeInMilis) {
		return Instant.ofEpochMilli(epochTimeInMilis).atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	/**
	 * takes a month and year and returns back a {@link LocalDate} set at first day of month
	 * @param month to set
	 * @param year to set
	 * @return ISO {@link LocalDate} of month, year, and first day of month (e.g. 2018-07-01)
	 */
	public static LocalDate toLocalDate(MONTH month, int year) {
		
		String dateString = year+"-"+month.month+"-01";       //ISO date e.g. "2018-07-14"
        
		return  LocalDate.parse( dateString );       
	}
	/**
	 * takes a month and year and returns back a {@link LocalDate} set at first day of month
	 * @param month to set
	 * @param year to set
	 * @return ISO {@link LocalDate} of month, year, and first day of month (e.g. 2018-07-01)
	 */
	public static LocalDate toLocalDate(Month month, int year) {
		
		String dateString = year+"-"+month.getValue()+"-01";       //ISO date e.g. "2018-07-14"
		
		return  LocalDate.parse(dateString);
	}
	
	/**
	 * check whether the given date lies between the given start and end dates
	 * 
	 * @param date to check
	 * @param start date to check
	 * @param end date to check
	 * @return true if given date is between the start and end dates
	 */
	public static boolean isBetween(LocalDate date, LocalDate start, LocalDate end) {
		return date.isBefore(end) && date.isAfter(start);
	}
	
	/**
	 * checks whether the input string matches the MM/YYYY format
	 * @param monthYearDateAsString to check
	 * @return true if string matches format
	 */
	public static boolean matchesMonthYearFormat(String monthYearDateAsString) {
		return monthYearRegexPattern.matcher(monthYearDateAsString).matches();
	}
	
}
