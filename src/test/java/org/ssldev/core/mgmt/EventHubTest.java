package org.ssldev.core.mgmt;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.junit.Test;
import org.ssldev.core.messages.FileReadMessage;
import org.ssldev.core.messages.LogMessage;

public class EventHubTest {
	EventHub hub = new EventHub();
	
	@Test public void registerMultipleConsumers() {
		final AtomicBoolean consumer1WasCalled = new AtomicBoolean(false);
		final AtomicBoolean consumer2WasCalled = new AtomicBoolean(false);
		Consumer<LogMessage> c1 = msg -> consumer1WasCalled.set(true);
		Consumer<LogMessage> c2 = msg -> consumer2WasCalled.set(true);
		hub.register(LogMessage.class, c1);
		hub.register(LogMessage.class, c2);
		hub.add(new LogMessage("blah"));
		
		assertTrue(consumer1WasCalled.get());
		assertTrue(consumer2WasCalled.get());
	}
	
	@Test public void registerDifferentTypeConsumers() {
		final AtomicBoolean consumer1WasCalled = new AtomicBoolean(false);
		final AtomicBoolean consumer2WasCalled = new AtomicBoolean(false);
		Consumer<LogMessage> c1 = msg -> consumer1WasCalled.set(true);
		Consumer<FileReadMessage> c2 = msg -> consumer2WasCalled.set(true);
		
		hub.register(LogMessage.class, c1);
		hub.register(FileReadMessage.class, c2);
		hub.add(new LogMessage("blah"));
		
		assertTrue(consumer1WasCalled.get());
		assertFalse(consumer2WasCalled.get());
	}
	@Test public void noConsumersRegistered() {
		final AtomicBoolean consumer2WasCalled = new AtomicBoolean(false);
		Consumer<FileReadMessage> c2 = msg -> consumer2WasCalled.set(true);
		
		hub.register(FileReadMessage.class, c2);
		hub.add(new LogMessage("blah"));
		
		assertFalse(consumer2WasCalled.get());
	}

}
