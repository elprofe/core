package org.ssldev.core.utils;

import java.io.Serializable;
import java.util.List;

public class SaveObject implements Serializable{
	private static final long serialVersionUID = 1L;
	String id;
	List<Serializable> data;
	
	public SaveObject(String id, List<Serializable> data) {
		this.id = id;
		this.data = data;
	}
}
