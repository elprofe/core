package org.ssldev.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * saves and loads files from disk.
 * TODO: turn into a service
 */
public class SaveUtil {
	private static SaveUtil INSTANCE;
	//TODO - save location cannot  be app specific in core
	private final String SAVE_LOC = SysInfo.getUserHomeLoc() + File.separator + ".sslApi" + File.separator;

	private SaveUtil() {
		// null
	}
	
	public static SaveUtil getInstance() {
		if(null == INSTANCE) {
			INSTANCE = new SaveUtil();
		}
		return INSTANCE;
	}

	
	/**
	 * save this data to disk.  if ID was previously used to save,
	 * data previously saved will be replaced with given data
	 *  
	 * @param key to save this data under
	 * @param data to be saved
	 */
	public void save(String key, Serializable... data) {
		Validate.notNull(data, "data cannot be null");
		Validate.notNull(key, "key cannot be null");
		
		Logger.info(this, "saving data to disk for " + key);
		
		serialize(key, Arrays.asList(data));
	}

	private void serialize(String key, List<Serializable> list) {
		SaveObject toWrite = new SaveObject(key,list);

		File saveFile = new File(getFilePath(key));
		try {
			// create save dir 
			if(!saveFile.getParentFile().exists()) {
				saveFile.getParentFile().mkdirs();
			} 
			// create save file 
			if(!saveFile.exists()) {
				saveFile.createNewFile();
			}
		}catch (Exception e) {
				Logger.error(this, "cannot create save file "+ saveFile.getAbsolutePath() + e);
				return;
		}
		
		// Serialize the original class object
		try {
			FileOutputStream fo = new FileOutputStream(saveFile, false); // overwrite existing file if exists
			ObjectOutputStream so = new ObjectOutputStream(fo);
			so.writeObject(toWrite);
			so.flush();
			so.close();
		} catch (Exception e) {
			Logger.error(this, "unable to save " + key +", due to " + e);
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * load previously saved data
	 * @param key to retrieve data
	 * @return list of saved data.  null if none found
	 */
	public List<Serializable> load(String key){
		Logger.info(this, "loading data for " + key);
		
		File serFile = new File(getFilePath(key));
		if(!serFile.exists() || !serFile.isFile()) {
			Logger.info(this, "cannot load file that does not exist or is not a file: " + key+".data");
			return null;
		}
		return deserialize(key);
	}
	
	private List<Serializable> deserialize(String key) {
		SaveObject toRead;
		
		try {
			FileInputStream fi = new FileInputStream(getFilePath(key));
			ObjectInputStream si = new ObjectInputStream(fi);
			toRead = (SaveObject) si.readObject();
			si.close();
		} catch (Exception e) {
			Logger.error(this, "unable to load " + key +", due to " + e);
			e.printStackTrace();
			return null;
		}
		
		if(null == toRead || toRead.data == null) {
			Logger.info(this, "no data found for " + key);
			return null;
		}
		
		return toRead.data;
	}
	
	private String getFilePath(String fileName) {
		return SAVE_LOC + fileName + ".data";
	}

	/**
	 * delete any files created for given key
	 * @param key of data to delete
	 */
	public void delete(String key) {
		
		try {
			File del = new File(getFilePath(key));
			del.delete();
		} catch (Exception e) {
			Logger.error(this, "unable to delete " + getFilePath(key));
		}
	}
	
}
