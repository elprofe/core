package org.ssldev.core.services;

import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.SysInfo;
/**
 * used to figure out various system info
 * <p>
 * Consumes: N/A 
 * <p>
 * Produces: N/A 
 */
public class SystemInfoValidatorService extends Service {

	public SystemInfoValidatorService(EventHub hub) {
		super(hub);
		
		logoutSysInfo();
	}

	private void logoutSysInfo() {
		StringBuilder sb = new StringBuilder("\nSystem Info:\n");
		// append system info
		sb.append("date/time: ").append(SysInfo.getCurrentDateAndTime()).append("\n");
		sb.append("java.runtime.name: ").append(System.getProperty("java.runtime.name")).append("\n");
		sb.append("user.country: ").append(System.getProperty("user.country")).append("\n");
		sb.append("user.dir: ").append(System.getProperty("user.dir")).append("\n");
		sb.append("java.runtime.version: ").append(System.getProperty("java.runtime.version")).append("\n");
		sb.append("java.version: ").append(System.getProperty("java.version")).append("\n");
		sb.append("os.arch: ").append(System.getProperty("os.arch")).append("\n");
		sb.append("os.name / os.version: ").append(System.getProperty("os.name")).append(" / ").append(System.getProperty("os.version")).append("\n");
		sb.append("file.encoding: ").append(System.getProperty("file.encoding")).append("\n");

		Logger.info(this, sb.toString());
	}

	@Override
	public void notify(MessageIF msg) {
		// do nothing...
	}
	


}
