package org.ssldev.core.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * utility to retrieve system info
 */
public class SysInfo {
	private static String OS = System.getProperty("os.name").toLowerCase();
	public static String USER_HOME_LIB;
	private static Date currDate = new Date();
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	public static boolean isWindows(){
		return (OS.indexOf("win")>=0);
	}

	public static boolean isMac(){
		return (OS.indexOf("mac")>=0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >=0 || OS.indexOf("nux") >=0 || OS.indexOf("aix") >= 0);
	}

	public static boolean isSolaris(){
		return (OS.indexOf("sunos") >=0);
	}
	
	public static String getUserHomeLoc() {
		if(USER_HOME_LIB != null) {
			return USER_HOME_LIB;
		}
		
		if(isMac()) {
			USER_HOME_LIB = (System.getProperty("user.home") + File.separator);
			
		}
		else if(isWindows()) {
			USER_HOME_LIB = (System.getenv("APPDATA") + File.separator);
		}
		else if(isUnix()) {
			Logger.warn(SysInfo.class, "unix/linux OS detected. though it should be okay, no testing was done on this OS... proceed with caution");
			USER_HOME_LIB = (System.getProperty("user.home") + File.separator);
		}
		else {
			throw new RuntimeException("OS is not currently supported");
		}
		
		return USER_HOME_LIB;
	}
	
	//TODO not core. app specific
	public static String getSslHomeLoc() {
		//TODO verify this works on windows
		return getUserHomeLoc() + "Music" + File.separator + "_Serato_" + File.separator;
	}
	
	public static int getNumAvailableThreads() {
		return Runtime.getRuntime().availableProcessors() + 1;
	}
	
	public static String getCurrentDateAndTime() {
		currDate.setTime(System.currentTimeMillis());
		return dateFormat.format(currDate);
	}
	
	/**
	 * get string representation of time in format: "MM/dd/yyyy HH:mm:ss"
	 * @param timeSinceEpoch to be stringified
	 * @return time in string format
	 */
	public static String getDate(long timeSinceEpoch) {
		currDate.setTime(timeSinceEpoch);
		return dateFormat.format(currDate);
	}
	
	/**
	 * get string representation of time in format: "mm:ss"
	 * @param timeInMilis to stringify
	 * @return time in string form
	 */
	public static String getTimeInMinutesSeconds(long timeInMilis) {
		return String.format("%02d:%02d", 
			    TimeUnit.MILLISECONDS.toMinutes(timeInMilis),
			    TimeUnit.MILLISECONDS.toSeconds(timeInMilis) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMilis))
			);
	}
}
