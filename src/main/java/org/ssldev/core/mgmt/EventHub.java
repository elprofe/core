package org.ssldev.core.mgmt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.services.ServiceIF;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.Validate;

/**
 * handles events pub/sub
 */
public class EventHub {
	/** registered services */
	protected Collection<ServiceIF> services = new LinkedList<ServiceIF>();

	/** used to filter message notifications to services */
	private Map<ServiceIF, List<Class<? extends MessageIF>>> msgNotifyRegistry = new HashMap<>();
	
	/** used to allow consumer function registrations for messages/events by type */
	private Map<Class<? extends MessageIF>, List<Consumer<MessageIF>>> msgConsumers = new HashMap<>();

	
	/** keep track of whether the hub has initialized */
	private boolean isInitialized;

	/** keep track of whether the hub has started */
	private boolean isStarted;
	
	/**
	 * register this service with the event hub to receive notifications and
	 * be able to add messages/events.  Optionally, a set of messages to be notified
	 * of can be given.
	 * 
	 * @param service registering
	 * @param msgsToRegisterFor messages to be notified of
	 */
	@SuppressWarnings("unchecked")
	public void register(ServiceIF service, Class<? extends MessageIF>... msgsToRegisterFor) {
		Validate.notNull(service, "service cannot be null");
		Validate.areClassesAssignableFrom(msgsToRegisterFor, MessageIF.class);
		
		Logger.info(this, "service registered: [" + service.getName() + "]");
		
		if(null != msgsToRegisterFor && msgsToRegisterFor.length != 0) {
			msgNotifyRegistry.put(service, Arrays.asList(msgsToRegisterFor));
		}
		
		services.add(service);
	}
	
	/**
	 * register a {@link Consumer} to be notified of events of the given type 
	 * @param <T> event/message type 
	 * @param type of event/message to be notified of
	 * @param eventConsumer to call on event
	 */
	@SuppressWarnings("unchecked")
	public <T extends MessageIF> void  register(Class<T> type, Consumer<T> eventConsumer) {
		msgConsumers.computeIfAbsent((Class<MessageIF>) type, t -> new ArrayList<>())
		 .add((Consumer<MessageIF>) eventConsumer);
	}
	
	/**
	 * determine if the given service wants to be notified of this particular 
	 * message
	 * 
	 * @param service
	 * @param msg
	 * @return true if service should be notified
	 */
	boolean isToBeNotified(ServiceIF service, MessageIF msg) {
		if(!msgNotifyRegistry.containsKey(service)) {
			// default to get every message
			return true;
		}
		
		return msgNotifyRegistry.get(service).contains(msg.getClass());
	}
	
	
	/**
	 * publishes the given message to to all applicable registered services.
	 * @param msg to add
	 */
	public void add(MessageIF msg) {
		
		// TODO remove service notification at some point.  each service should register a message consumer
		// to consume a message-of-interest
		new ArrayList<>(services).stream().forEach(s-> 
		{
			if(isToBeNotified(s, msg)) s.notify(msg); 
		});
		
		// notify message consumers 
		List<Consumer<MessageIF>> list = msgConsumers.get(msg.getClass());
		if(list != null) {
			list.forEach(c -> c.accept(msg));
		}
	}
	

	/**
	 * "discovers" a service that is assignable from the given class
	 * @param clz to discover
	 * @return {@link Optional} service.  or empty if not found
	 */
	public Optional<ServiceIF> discover(Class<? extends ServiceIF> clz) {
		return services.stream()
				.filter(s -> clz.isAssignableFrom(s.getClass()))
				.findFirst();
	}

	public void invokeLater(Runnable task) {
		// to be overriden if different execution is needed
		task.run();
	}

	/**
	 * notify each registered service to 'initialize'
	 */
	public void init() {
		if(!isInitialized) {
			isInitialized = true;
			services.forEach(s-> s.init());
		}
	}
	/**
	 * notify each registered service that 'start' is occurring
	 */
	public void start() {
		if(!isStarted) {
			isStarted = true;
			for(ServiceIF serv : services) {
				serv.start();
			}
//			services.forEach(s-> s.start());
		}
	}
	
	/**
	 * notify each registered service that 'shutdown' is occurring
	 */
	public void shutdown() {
		services.forEach(s-> s.shutdown());
	}

	/**
	 * de-register service from hub.
	 * 
	 * @param service to de-register
	 */
	public void deregister(ServiceIF service) {
		services.remove(service);
	}

}
