package org.ssldev.core.messages;

import java.io.File;

/**
 * write content to said file 
 */
public class WriteToFileMessage extends Message {
	public String fileloc;
	public String content;
	
	public WriteToFileMessage(String loc, String c) {
		if(null == loc) throw new IllegalArgumentException("file location is null");
		if(null == c) throw new IllegalArgumentException("content is null");
		if(!new File(loc).exists()) throw new IllegalArgumentException("file does not exist: "+loc);
		
		fileloc = loc;
		content = c;
	}
	
	@Override
	public String toString() {
		return super.toString() + fileloc;
	}
}
