package org.ssldev.core.messages;

import java.io.File;

import org.ssldev.core.utils.Logger;

/**
 * reads in file contents into a list of strings
 */
public class ReadFromFileMessage extends Message {

	public File fileToRead;

	public ReadFromFileMessage(File f) {
		this.fileToRead = f;
		if(f==null || !f.isFile()) {
			Logger.warn(this, "cannot read from file " + (f==null?"null" : f.getAbsolutePath()));
		}
	}
}
